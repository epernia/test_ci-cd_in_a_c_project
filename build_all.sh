#!/bin/bash
#if test -d gcc-arm-none-eabi-9-2019-q4-major
#then
#  rm -R gcc-arm-none-eabi-9-2019-q4-major
#fi
#tar -xjf gcc-arm-none-eabi-9-2019-q4-major-x86_64-linux.tar.bz2
mbed config -G GCC_ARM_PATH `pwd`/gcc-arm-none-eabi-9-2019-q4-major/bin
mbed import mbed-os || true
mbed config -G MBED_OS_DIR mbed-os || true 
mkdir -p binaries 
#ex=`ls -d ex*`
ex=`cat prjList.dat`
for eachEx in $ex
do
  if test -d $eachEx
  then
    mbed new $eachEx
    line="mbed compile -m NUCLEO_F429ZI -t GCC_ARM --source $eachEx --source mbed-os " 
    cd $eachEx
    if test -d modules
    then
      cd modules
      mod=`ls`
      for m in $mod
      do
         if test -d $m 
         then
           line="$line --source $eachEx/modules/$m"
         fi
      done
      cd ..
    fi
    cd ..
  echo "Executing: $line"
  eval $line
  mv BUILD/NUCLEO_F429ZI/GCC_ARM/$eachEx.bin binaries/
  mv BUILD/NUCLEO_F429ZI/GCC_ARM/$eachEx.elf binaries/
  mv BUILD/NUCLEO_F429ZI/GCC_ARM/$eachEx.map binaries/
  fi
done
